/****************************************************************************
**
** Copyright (C) 2020 Gianni Peschiutta (F4IKZ).
** Contact: https://bitbucket.org/Artemia/opengd77cps/admin/issues
**
** opengd77cps is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** opengd77cps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** The license is as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this software. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html.
****************************************************************************/

/****************************************************************************
 * mainwindow.cpp is part of opengd77cps project
 *
 * This is the main window class for opengd77cps application
 * **************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QMenu>
#include <QFileDialog>
#include <QTextStream>
#include <QMdiSubWindow>

#include "gui/general.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // Specify custom handler for contextual menu
    ui->pteLog->setContextMenuPolicy(Qt::CustomContextMenu);
    // Init window position and size
    m_settings.beginGroup("MainWindow");
    restoreGeometry(m_settings.value("Geometry").toByteArray());
    restoreState(m_settings.value("State").toByteArray());
    m_settings.endGroup();
    m_settings.beginGroup("log");
    m_maxLine = m_settings.value("MaxLine",200).toInt();
    m_settings.endGroup();
    QPalette p = ui->pteLog->palette();
    p.setColor(QPalette::Text,Qt::darkBlue);
    m_log = CLogger::instance();
    connect(m_log, SIGNAL(fireLog(QString,QColor,CL_DEBUG_LEVEL)),this,SLOT(onLog(QString,QColor,CL_DEBUG_LEVEL)),Qt::QueuedConnection);
    ui->pteLog->setPalette(p);
    ui->pteLog->setMaximumBlockCount(m_maxLine);
    m_log->log(tr("Welcome on openGD77 Code Plug Software"),Qt::blue);
#ifdef QT_DEBUG
    this->setWindowTitle(this->windowTitle() + tr(" Debug Mode"));
    m_log->setDebugLevel(LEVEL_VERBOSE);
#endif
    ui->mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    QMdiSubWindow* subGeneral = new QMdiSubWindow;
    subGeneral->setWidget(new General(subGeneral));
    ui->mdiArea->addSubWindow(subGeneral);
    m_serialCtrl = SerialPortCtrl::instance();
}

MainWindow::~MainWindow()
{
    disconnect(m_log, SIGNAL(fireLog(QString,QColor,CL_DEBUG_LEVEL)),this,SLOT(onLog(QString,QColor,CL_DEBUG_LEVEL)));
    delete ui;
}

///
/// \brief MainWindow::onLog
/// \param Texte
/// \param pColor
/// \param pLevel
///
/// Redirect Log Text flow to Plain Text Edit on main Window
///
void MainWindow::onLog (const QString& pMessage, QColor pColor, CL_DEBUG_LEVEL)
{
    QTextCharFormat fmt;
    fmt.setForeground(QBrush(pColor));
    ui->pteLog->mergeCurrentCharFormat(fmt);
    ui->pteLog->appendPlainText(pMessage);
}

///
/// \brief MainWindow::closeEvent
///
void MainWindow::closeEvent(QCloseEvent*)
{
    m_settings.beginGroup("MainWindow");
    m_settings.setValue("Geometry",saveGeometry());
    m_settings.setValue("State",saveState());
    m_settings.endGroup();
    m_settings.beginGroup("Log");
    m_settings.setValue("MaxLine",m_maxLine);
    m_settings.endGroup();
}

///
/// \brief MainWindow::clearLog
///
void MainWindow::clearLog()
{
    ui->pteLog->clear();
}

///
/// \brief MainWindow::saveLog
///
void MainWindow::saveLog()
{
    QFileDialog FileDlg(this);
    QString File;
    FileDlg.setAcceptMode(QFileDialog::AcceptSave);
    FileDlg.setDefaultSuffix("htm");
    FileDlg.setNameFilter(tr("Web File (*.htm *.html)"));
    if (FileDlg.exec())
    {
        File = FileDlg.selectedFiles().first();
        if (!File.isEmpty())
        {
            QTextDocument* Doc = ui->pteLog->document();
            QFile document (File);
            if(!document.open(QFile::WriteOnly | QFile::Text))
            {
                m_log->log(tr("An Error occur while opening ") + document.fileName(),Qt::darkMagenta);
                return;
            }
            QTextStream writer (&document);
            writer << Doc->toHtml();
        }
    }
}

///
/// \brief MainWindow::on_pteLog_customContextMenuRequested
/// \param pos
///
void MainWindow::on_pteLog_customContextMenuRequested(const QPoint &pos)
{
    QMenu *menu = ui->pteLog->createStandardContextMenu();
    // Handle global position
    QPoint globalPos = ui->pteLog->mapToGlobal(pos);
    menu->addAction(tr("Clear"),this,SLOT(clearLog()));
    menu->addAction(tr("Save"),this,SLOT(saveLog()));
    menu->exec(globalPos);
    delete menu;
}

void MainWindow::on_actionRead_CodePlug_triggered()
{
    m_serialCtrl->Start(CommType::codeplugRead);
}
