/****************************************************************************
**
** Copyright (C) 2020 Gianni Peschiutta (F4IKZ).
** Contact: https://bitbucket.org/Artemia/opengd77cps/admin/issues
**
** opengd77cps is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** opengd77cps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** The license is as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this software. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html.
****************************************************************************/

/****************************************************************************
 * mainwindow.h is part of opengd77cps project
 *
 * This is the main window class for opengd77cps application
 * **************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextCodec>
#include <QSettings>
#include <QTimer>
#include <QListWidget>

#include "tools/clogger.h"
#include "comm/serialportctrl.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void        onLog(const QString& pMessage,QColor pColor, CL_DEBUG_LEVEL pLevel);
    void        clearLog();
    void        saveLog();
    void        on_pteLog_customContextMenuRequested(const QPoint &pos);

    void on_actionRead_CodePlug_triggered();

private:
    Ui::MainWindow* ui;
    CLogger*        m_log;
    SerialPortCtrl* m_serialCtrl;
    QSettings       m_settings;
    int             m_maxLine;
    void            closeEvent(QCloseEvent* pEvent);
};
#endif // MAINWINDOW_H
