/****************************************************************************
**
** Copyright (C) 2020 Gianni Peschiutta (F4IKZ).
** Contact: https://bitbucket.org/Artemia/opengd77cps/admin/issues
**
** opengd77cps is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** opengd77cps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** The license is as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this software. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html.
****************************************************************************/

/****************************************************************************
 * general.cpp is part of opengd77cps project
 *
 * This is the MDI children for generals settings of OpenGD77 codeplug
 * **************************************************************************/

#include "general.h"
#include "ui_general.h"

General::General(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::General)
{
    ui->setupUi(this);
}

General::~General()
{
    delete ui;
}
