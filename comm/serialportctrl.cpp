/****************************************************************************
**
** Copyright (C) 2020 Gianni Peschiutta (F4IKZ).
** Contact: https://bitbucket.org/Artemia/opengd77cps/admin/issues
**
** opengd77cps is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** opengd77cps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** The license is as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this software. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html.
****************************************************************************/

/****************************************************************************
 * serialportctrl.cpp is part of opengd77cps project
 *
 * This is the singleton controller for serial port communication
 * **************************************************************************/

#include "serialportctrl.h"
#include "tools/singleton.h"

#include <QDataStream>

SerialPortCtrl::SerialPortCtrl(QObject *parent) :
    QObject(parent)
{
    qRegisterMetaType<CommType>("CommType");
    qRegisterMetaType<CommRes>("CommRes");
    m_log = CLogger::instance();
    m_thread = new QThread();
    //detectRadioSerialPort();
    connect(m_thread, SIGNAL(started()), this, SLOT(mainThread()),Qt::DirectConnection);
    connect(m_thread, SIGNAL(finished()), this, SLOT(threadFinished()),Qt::DirectConnection );
}

SerialPortCtrl::~SerialPortCtrl()
{

}

///
/// \brief CLogger::createInstance
/// \return
///
SerialPortCtrl* SerialPortCtrl::createInstance()
{
    return new SerialPortCtrl();
}

///
/// \brief CLogger::instance
/// \return
///
SerialPortCtrl* SerialPortCtrl::instance()
{
    return Singleton<SerialPortCtrl>::instance(SerialPortCtrl::createInstance);
}

CommRes SerialPortCtrl::detectRadioSerialPort()
{
    const QList<QSerialPortInfo> infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
    {
#ifdef QT_DEBUG
        QString s = tr("Port: ") + info.portName() + "\n"
                            + QObject::tr("Location: ") + info.systemLocation() + "\n"
                            + QObject::tr("Description: ") + info.description() + "\n"
                            + QObject::tr("Manufacturer: ") + info.manufacturer() + "\n"
                            + QObject::tr("Serial number: ") + info.serialNumber() + "\n"
                            + QObject::tr("Vendor Identifier: ") + (info.hasVendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : QString()) + "\n"
                            + QObject::tr("Product Identifier: ") + (info.hasProductIdentifier() ? QString::number(info.productIdentifier(), 16) : QString()) + "\n";
        m_log->log(s,Qt::darkBlue,LEVEL_VERBOSE);
#endif
        if (info.hasVendorIdentifier()&&info.hasProductIdentifier())
        {
            QString Device;
            Device.sprintf("%04X:%04X",info.vendorIdentifier(), info.productIdentifier());
            if (Device == GD77_CPS_ID)
            {
                m_device = info;
#ifdef QT_DEBUG
                m_log->log(tr("GD-77 Detected in CPS Mode"), Qt::darkBlue, LEVEL_NORMAL);
#endif
                return CommRes::Success;
            }
        }
    }
    m_log->log(tr("No GD-77 Found"),Qt::darkRed);
    return CommRes::NoDevice;
}

CommRes SerialPortCtrl::Start(const CommType& pType)
{
    CommRes Result=CommRes::Success;
    m_log->log(tr("Starting CodePlug Operation ..."));
    if (m_thread->isRunning())
    {
        m_log->log(tr("Failed to start, Serial port is actually busy."));
        return CommRes::Busy;
    }
    m_commande = pType;
    m_thread->start();
    return Result;
}

void SerialPortCtrl::mainThread()
{
    if(m_ser==nullptr) m_ser = new QSerialPort(m_thread);
    if ((m_threadResult = detectRadioSerialPort()) == Success)
    {
        m_ser->setPortName(m_device.portName());
        m_ser->setBaudRate(QSerialPort::Baud115200);
        m_ser->setStopBits(QSerialPort::OneStop);
        m_ser->setParity(QSerialPort::NoParity);
        m_ser->setDataBits(QSerialPort::Data8);
        //m_ser->setFlowControl(QSerialPort::NoFlowControl);
        if (m_ser->open(QIODevice::ReadWrite))
        {
            switch (m_commande)
            {
                case CommType::codeplugRead:
                {
                    readCodePlug();
                    break;
                }
                case CommType::codeplugWrite:
                {
                    break;
                }
            }
        }
        else
        {
            m_log->log(tr("Failed to open port ") + m_device.portName() +" :" + m_ser->errorString(),Qt::darkRed);
            m_threadResult=CommRes::ReadFailed;
        }
    }
    if (m_ser->isOpen()) m_ser->close();
    delete m_ser;
    m_ser=nullptr;
    m_thread->exit();
}

void SerialPortCtrl::threadFinished()
{
    if (m_threadResult != CommRes::Success)
    {
        m_log->log(tr("CodePlug Operation Aborted"),Qt::darkRed);
    }
    emit (finished(m_threadResult));
}

void SerialPortCtrl::readCodePlug()
{
    OpenGD77CommsTransferData dataObj(OpenGD77CommsTransferData::CommsAction::READ_CODEPLUG);
    const quint32 CODEPLUG_FLASH_PART_END	= 0x1EE60;
    const quint32 CODEPLUG_FLASH_PART_START = 0xB000;
    if (!sendCommand(0))
    {
        dataObj.responseCode = 1;
        m_log->log(tr("Error connecting to the OpenGD77"),Qt::darkRed);
        m_threadResult = CommRes::ReadFailed;
        return;
    }
    sendCommand(1);
    sendCommand(2,0,0,3,1,0,"CPS");// Write a line of text to CPS screen at position x=0,y=3 with font size 3, alignment centre
    sendCommand(2,0,16,3,1,0, "Reading");// Write a line of text to CPS screen
    sendCommand(2,0,32,3,1,0, "Codeplug");// Write a line of text to CPS screen
    sendCommand(3);// render CPS
    sendCommand(6,3);// flash green LED
    sendCommand(6, 2);// Save settuings VFO's to codeplug
    dataObj.mode = OpenGD77CommsTransferData::CommsDataMode::DataModeReadEEPROM;
    dataObj.localDataBufferStartPosition = 0x00E0;
    dataObj.startDataAddressInTheRadio = dataObj.localDataBufferStartPosition;
    dataObj.transferLength =  0x6000 - dataObj.localDataBufferStartPosition;
    m_log->log(QString::asprintf("Reading EEPROM 0x%06X - 0x%06X", dataObj.localDataBufferStartPosition, (dataObj.localDataBufferStartPosition + dataObj.transferLength)));
    if (!ReadFlashOrEEPROMOrROMOrScreengrab(dataObj))
    {
        m_log->log(tr("Error while reading"),Qt::darkRed);
        dataObj.responseCode = 1;
        m_threadResult = CommRes::ReadFailed;
        return;
    }
    dataObj.mode = OpenGD77CommsTransferData::CommsDataMode::DataModeReadEEPROM;
    dataObj.localDataBufferStartPosition = 0x7500;
    dataObj.startDataAddressInTheRadio = dataObj.localDataBufferStartPosition;
    dataObj.transferLength =  0xB000 - dataObj.localDataBufferStartPosition;
    m_log->log(QString::asprintf("Reading EEPROM 0x%06X - 0x%06X", dataObj.localDataBufferStartPosition, (dataObj.localDataBufferStartPosition + dataObj.transferLength)));
    if (!ReadFlashOrEEPROMOrROMOrScreengrab(dataObj))
    {
        m_log->log(tr("Error while reading"),Qt::darkRed);
        dataObj.responseCode = 1;
        m_threadResult = CommRes::ReadFailed;
        return;
    }
    dataObj.mode = OpenGD77CommsTransferData::CommsDataMode::DataModeReadEEPROM;
    dataObj.localDataBufferStartPosition = CODEPLUG_FLASH_PART_START;
    dataObj.startDataAddressInTheRadio = 0x7b000;
    dataObj.transferLength =  CODEPLUG_FLASH_PART_END - dataObj.localDataBufferStartPosition;
    m_log->log(QString::asprintf("Reading Flash 0x%06X - 0x%06X", dataObj.localDataBufferStartPosition, (dataObj.localDataBufferStartPosition + dataObj.transferLength)));
    if (!ReadFlashOrEEPROMOrROMOrScreengrab(dataObj))
    {
        m_log->log(tr("Error while reading"),Qt::darkRed);
        dataObj.responseCode = 1;
        m_threadResult = CommRes::ReadFailed;
        return;
    }
    dataObj.mode = OpenGD77CommsTransferData::CommsDataMode::DataModeReadEEPROM;
    dataObj.localDataBufferStartPosition = 0x1EE60;
    dataObj.startDataAddressInTheRadio = 0;
    dataObj.transferLength =  0x20000 - 0x1EE60;
    m_log->log(QString::asprintf("Reading Flash 0x%06X - 0x%06X", dataObj.localDataBufferStartPosition, (dataObj.localDataBufferStartPosition + dataObj.transferLength)));
    if (!ReadFlashOrEEPROMOrROMOrScreengrab(dataObj))
    {
        m_log->log(tr("Error while reading"),Qt::darkRed);
        dataObj.responseCode = 1;
        m_threadResult = CommRes::ReadFailed;
        return;
    }
    else
    {
        m_log->log(tr("Codeplug read complete"));
    }
    sendCommand(5);// close CPS screen on radio
    m_data = dataObj;
    m_threadResult=CommRes::Success;
}

void SerialPortCtrl::SerialError(QSerialPort::SerialPortError pErr)
{
    m_log->log(tr("Serial port error :  ") + QString::number(pErr) +" :" ,Qt::darkRed);
}

bool SerialPortCtrl::sendCommand(quint8 commandNumber, quint8 x_or_command_option_number, quint8 y,quint8 iSize, quint8 alignment,quint8 isInverted,QString Message)
{
    int retries = 100;
    QByteArray buffer(64,0x00);
    QDataStream data(&buffer, QIODevice::WriteOnly);
    data << static_cast<quint8>('C');
    data << commandNumber;
    switch (commandNumber)
    {
        case 2:
            data << static_cast<quint8>(0x00);
            data << y;
            data << iSize;
            data << alignment;
            data << isInverted;
            data.writeRawData( Message.toStdString().c_str(),Message.length()+1);
            break;
        case 6:
            data << x_or_command_option_number;
            break;
        default:
            break;
    }
    //while(buffer.size()< 32) data << static_cast<quint8>(0);
    m_ser->write(buffer,32);
    while (m_ser->bytesAvailable() ==0 && retries-- > 0)
    {
        m_ser->waitForReadyRead(1);
    }

    if (retries != -1)
    {
        buffer = m_ser->read(64);
    }
    return ((static_cast<quint8>(buffer[1]) == commandNumber) && (retries !=-1));
}

bool SerialPortCtrl::ReadFlashOrEEPROMOrROMOrScreengrab(OpenGD77CommsTransferData& dataObj)
{
    int old_progress = 0;
    QByteArray sendbuffer (512,0x00);
    QDataStream senddata (&sendbuffer,QIODevice::WriteOnly);
    senddata.setByteOrder(QDataStream::BigEndian);
    QByteArray readbuffer (512,0x00);
    QByteArray com_buf (512,0x00);
    quint32 currentDataAddressInTheRadio = dataObj.startDataAddressInTheRadio;
    quint32 currentDataAddressInLocalBuffer = dataObj.localDataBufferStartPosition;
    quint16 size = (dataObj.startDataAddressInTheRadio + dataObj.transferLength) - currentDataAddressInTheRadio;
    while (size > 0)
    {
        if (size > MAX_TRANSFER_SIZE)
        {
            size = MAX_TRANSFER_SIZE;
        }

        senddata << static_cast<quint8>('R');
        senddata << static_cast<quint8>(dataObj.mode);
        senddata << currentDataAddressInTheRadio;
        senddata << size;
        m_ser->write(sendbuffer,8);
        m_ser->waitForReadyRead();
        readbuffer = m_ser->read(64);
        if (readbuffer[0] == 'R')
        {
            quint16 len = (readbuffer[1] << 8) + (readbuffer[2] << 0);
            for (int i = 0; i < len; i++)
            {
                dataObj.dataBuff[currentDataAddressInLocalBuffer++] = readbuffer[i + 3];
            }

            int progress = (currentDataAddressInTheRadio - dataObj.startDataAddressInTheRadio) * 100 / dataObj.transferLength;
            if (old_progress != progress)
            {
                //updateProgess(progress);
                old_progress = progress;
            }

            currentDataAddressInTheRadio = currentDataAddressInTheRadio + len;
        }
        else
        {
            m_log->log(tr("read stopped (error at ") + QString::asprintf("%08X",currentDataAddressInTheRadio), Qt::darkRed);
            return false;
        }
        size = (dataObj.startDataAddressInTheRadio + dataObj.transferLength) - currentDataAddressInTheRadio;
    }
    return true;
}
