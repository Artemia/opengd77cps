/****************************************************************************
**
** Copyright (C) 2020 Gianni Peschiutta (F4IKZ).
** Contact: https://bitbucket.org/Artemia/opengd77cps/admin/issues
**
** opengd77cps is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** opengd77cps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** The license is as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this software. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html.
****************************************************************************/

/****************************************************************************
 * serialportctrl.cpp is part of opengd77cps project
 *
 * This is the singleton controller for serial port communication
 * **************************************************************************/

#ifndef SERIALPORTCTRL_H
#define SERIALPORTCTRL_H

#include <QObject>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QThread>
#include <QMetaEnum>

#include "tools/clogger.h"
#include "comm/opengd77commstransferdata.h"

#define GD77_CPS_ID "1FC9:0094"
#define GD77_FW_ID "15A2:0073"
#define MAX_TRANSFER_SIZE 32

enum CommType
{
    codeplugRead,
    codeplugWrite
};

enum CommRes
{
    Success,
    NoDevice,
    ReadFailed,
    WriteFailed,
    Busy
};

class SerialPortCtrl : public QObject
{
    Q_OBJECT

private:
                            SerialPortCtrl(QObject *parent = nullptr);
    static SerialPortCtrl*  createInstance();
    CommRes                 detectRadioSerialPort();
    void                    readCodePlug();
    void                    writeCodePlug();
    bool                    sendCommand(quint8 commandNumber, quint8 x_or_command_option_number =0, quint8 y=0,quint8 iSize=0, quint8 alignment =0, quint8 isInverted=0,QString Message="");
    bool                    ReadFlashOrEEPROMOrROMOrScreengrab(OpenGD77CommsTransferData& dataObj);
    CLogger*                m_log=nullptr;
    QSerialPortInfo         m_device;
    QThread*                m_thread=nullptr;
    QMetaEnum               m_metaCommType;
    QMetaEnum               m_metaCommResult;
    CommType                m_commande;
    CommRes                 m_threadResult;
    QSerialPort*            m_ser=nullptr;
    OpenGD77CommsTransferData m_data;

public:
                            ~SerialPortCtrl();
    static SerialPortCtrl*  instance();
    CommRes                 Start(const CommType& pType);

private slots:
    void                    mainThread();
    void                    threadFinished();
    void                    SerialError(QSerialPort::SerialPortError);

signals:
    void                    finished(CommRes);
};

#endif // SERIALPORTCTRL_H
