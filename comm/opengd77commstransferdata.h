#ifndef OPENGD77COMMSTRANSFERDATA_H
#define OPENGD77COMMSTRANSFERDATA_H

#include <QtGlobal>
#include <QByteArray>

class OpenGD77CommsTransferData
{
public:
    enum CommsDataMode
    {
        DataModeNone = 0,
        DataModeReadFlash,
        DataModeReadEEPROM,
        DataModeWriteFlash,
        DataModeWriteEEPROM,
        DataModeReadMCUROM,
        DataModeReadScreenGrab
    };
    enum CommsAction
    {
        NONE,
        BACKUP_EEPROM,
        RESTORE_EEPROM,
        BACKUP_FLASH,
        RESTORE_FLASH,
        BACKUP_CALIBRATION,
        RESTORE_CALIBRATION,
        READ_CODEPLUG,
        WRITE_CODEPLUG,
        BACKUP_MCU_ROM,
        DOWLOAD_SCREENGRAB
    };

    OpenGD77CommsTransferData();
    OpenGD77CommsTransferData(CommsAction theAction);

    CommsDataMode mode;
    CommsAction action;
    quint32 startDataAddressInTheRadio = 0;
    quint32 transferLength = 0;
    quint32 localDataBufferStartPosition = 0;
    quint32 data_sector = 0;
    QByteArray dataBuff;
    quint32 responseCode = 0;


};

#endif // OPENGD77COMMSTRANSFERDATA_H
