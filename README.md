# OpenGD77CPS

This project is the portage of VK3KYY OpenGD-77 CPS written in C# you can get on URL [here](https://github.com/rogerclarkmelbourne/radioddity_gd-77_cps)

OpenGP77CPS claim to be cross platform and open source.

It  will be designed in C++ with Qt library. The low level needs were use as most as possible embeded Qt and STL (Boost) Tools to keep portability.

OpenGP77CPS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

OpenGP77CPS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

The license is as published by the Free Software
Foundation and appearing in the file LICENSE
included in the packaging of this software. Please review the following
information to ensure the GNU General Public License requirements will
be met: [GPLv2 licence ](https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html)

1. DEPENDENCIES

	- Qt : >=5.9.5

1. Build on Linux
	1. UBUNTU

		On ubuntu you can enter this command line how provide you all dependencies needed to build OpenGP77CPS:

			sudo apt-get install build-essential qt5-default pkg-config

		On some qt installation you will need opengl devel :

			sudo apt-get install mesa-common-dev

	1. Gentoo / Calculate Linux

		Prepare dependencies on terminal as superuser:

			emerge @qt5-essentials

	1. Arch Linux

		Prepare dependencies on terminal :

			sudo yaourt -S base-devel gdb qt5-base

	1. Final build:

		on git root (not as root user):

			qmake CONFIG+=release
			make -j3
			./release/opengd77cps

	1. Update

		repeat previous commands after git pull

1. WIN(Visual studio community edition)

	coming ...