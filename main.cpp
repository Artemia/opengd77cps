/****************************************************************************
**
** Copyright (C) 2020 Gianni Peschiutta (F4IKZ).
** Contact: https://bitbucket.org/Artemia/opengd77cps/admin/issues
**
** opengd77cps is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** opengd77cps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** The license is as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this software. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html.
****************************************************************************/

/****************************************************************************
 * main.cpp is part of opengd77cps project
 *
 * This is the entry point of opengd77cps application
 * **************************************************************************/

#include "gui/mainwindow.h"
#include <QApplication>
#include <QSettings>
#include <QTranslator>
#include <QFile>
#include <QDir>

///
/// \brief main
/// \param argc
/// \param argv
/// \return
///
int main(int argc, char *argv[])
{
    QSettings::setDefaultFormat(QSettings::NativeFormat);
    QCoreApplication::setOrganizationName("F4IKZ");
    QCoreApplication::setApplicationName("OpenGD77CPS");
    QString locale = QLocale::system().name();
    QApplication app(argc, argv);
    QDir::setCurrent(app.applicationDirPath());
    QTranslator translator;
#ifdef Q_OS_LINUX
    translator.load("/usr/share/opengd77cps/translations/opengd77cps_" + locale +".qm");
    QFile styleFile ("/usr/share/opengd77cps/opengd77.qss");
#else
#ifdef Q_OS_MACOS
    translator.load("../Resources/opengd77cps_"+ locale +".qm");
    QFile styleFile ("../Resources/opengd77cps.qss");
#else
    translator.load("translations/opengd77cps_" + locale +".qm");
    QFile styleFile ( "opengd77cps.qss");
#endif
#endif
    app.installTranslator(&translator);
    //Load Style Sheet
    styleFile.open(QFile::ReadOnly);
    if (styleFile.isOpen())
    {
        app.setStyleSheet(styleFile.readAll());
        styleFile.close();
    }
    MainWindow mainWindow;
    mainWindow.show();
    return app.exec();
}
