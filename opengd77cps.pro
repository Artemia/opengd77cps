QT += core widgets gui xml serialport

CONFIG += build_all c++14
CONFIG -= debug_and_release debug_and_release_target

TARGET = opengd77cps
TEMPLATE = app

TARGET_NAME=opengd77cps

TRANSLATIONS += \
    intl/opengd77cps_en_US.ts

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    comm/opengd77commstransferdata.cpp \
    comm/serialportctrl.cpp \
    main.cpp \
    gui/mainwindow.cpp \
    gui/general.cpp \
    tools/clogger.cpp

HEADERS += \
    comm/opengd77commstransferdata.h \
    comm/serialportctrl.h \
    gui/mainwindow.h \
    gui/general.h \
    tools/call_once.h \
    tools/clogger.h \
    tools/singleton.h

FORMS += \
    gui/mainwindow.ui \
    gui/general.ui

RC_ICONS = OpenGD77.ico

DISTFILES += \
    LICENCE \
    README.md

RESOURCES += \
    resources.qrc
